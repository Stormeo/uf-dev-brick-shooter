﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawnerScript : MonoBehaviour
{
    public GameObject BossPrefab;

    private int Sessionkill = 0;

    private bool one = false;

    // Start is called before the first frame update
    void Start()
    {
            
    }

    private void Update()
    {
        Sessionkill = PlayerPrefs.GetInt("Sessionkill");

        if (Sessionkill >= 40)
        {
            if (!one)
            {
                SpawnBoss();
                one = true;
            }
        }
    }

    // Update is called once per frame
    void SpawnBoss()
    {

        float pos_X = 0f;
        Vector2 temp = transform.position;
        temp.x = pos_X;

        Instantiate(BossPrefab, temp, Quaternion.Euler(0f, 0f, 180f));
    }
}

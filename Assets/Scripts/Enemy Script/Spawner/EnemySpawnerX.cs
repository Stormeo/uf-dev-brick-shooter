﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerX : MonoBehaviour
{
    public float min_X, max_X;

    public GameObject enemyPrefab;

    private int Sessionkill = 0;

    public float timer = 4f;

    // Start is called before the first frame update
    void Start()
    {
        
        Invoke("SpawnEnemies", timer);
    }

    private void Update()
    {
        Sessionkill = PlayerPrefs.GetInt("Sessionkill");

        if(Sessionkill < 10)
        {
            timer = 5f;
        }else if (Sessionkill >= 10 && Sessionkill < 25)
        {
            timer = 4f;
        }else if (Sessionkill >= 25 && Sessionkill < 40)
        {
            timer = 3f;
        }else if (Sessionkill >= 40 )
        {
            timer = 7f;
        }
    }

    // Update is called once per frame
    void SpawnEnemies()
    {

        float pos_X = Random.Range(min_X, max_X);
        Vector2 temp = transform.position;
        temp.x = pos_X;

        Instantiate(enemyPrefab, temp, Quaternion.Euler(0f, 0f, 180f));

        Invoke("SpawnEnemies", timer);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerYLeft : MonoBehaviour
{

    public float min_Y, max_Y;

    public GameObject enemyPrefab;

    private int Sessionkill;
    private bool active;
    private bool one;

    private float rotation = -90f;

    public float timer = 2f;

    // Start is called before the first frame update
    void Start()
    {
        one = false;
    }

    private void Update()
    {
        Sessionkill = PlayerPrefs.GetInt("Sessionkill");

        if (Sessionkill >= 10 && Sessionkill < 25)
        {
            active = true;
            timer = 7f;
            if (!one)
            {
                Invoke("SpawnEnemies", timer);
                one = true;
            }
        }
        else if (Sessionkill >= 25 && Sessionkill < 40)
        {
            timer = 5f;
        }
        else if (Sessionkill >= 40)
        {
            timer = 8f;
        }
    }

    void SpawnEnemies()
    {

        if (active)
        {
            float pos_Y = Random.Range(min_Y, max_Y);
            Vector2 temp = transform.position;
            temp.y = pos_Y;

            Instantiate(enemyPrefab, temp, Quaternion.Euler(0f, 0f, rotation));

            Invoke("SpawnEnemies", timer);
        }

    }
}

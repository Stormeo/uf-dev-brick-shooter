﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public float speed = 5f;
    public float rotate_Speed = 50f;
    public float randomtest = 0f;

    public bool canShoot;
    public bool canRotate;
    public bool canMove = true;

    private bool canBoostLife = true;
    private bool canBoostSpeed = true;
    private bool canBoostFire = true;
    private float RandomBoost;

    public float bound_Y = -6f;

    public Transform attack_point;
    public GameObject bulletPrefab;

    public GameObject boostlifeprefab;
    public GameObject boostspeedprefab;
    public GameObject boostfireprefab;



    private Animator anim;
    private AudioSource explosionSound; //A Monter Dans le Future


    // Start is called before the first frame update
    void Awake() {
        anim = GetComponent<Animator>();
        explosionSound = GetComponent<AudioSource>();
        
    }

    void Start() {
        if(canRotate) {
            if(Random.Range(0, 2) > 0) {
                rotate_Speed = Random.Range(rotate_Speed, rotate_Speed + 20f);
                rotate_Speed *= -1f;
            } else {
                rotate_Speed = Random.Range(rotate_Speed, rotate_Speed + 20f);
            }
        }

        if (canShoot)
        {
            Invoke("StartShooting", randomtest);
            Invoke("Anim_shoot", randomtest);
        }
        /*
        if (PlayerPrefs.GetInt("bonusLife_sellVerif") != 0)
        {
            canBoostLife = true;
        }

        if (PlayerPrefs.GetInt("bonusSpeed_sellVerif") != 0)
        {
            canBoostSpeed = true;
        }

        if (PlayerPrefs.GetInt("bonusFire_sellVerif") != 0)
        {
            canBoostFire = true;
        }*/

    }


    // Update is called once per frame
    void Update() {
        Move();
        //RotateEnemy();
        anim.SetBool("Enemy_fire", false);
    }

    void Move() {
        if(canMove) {
            Vector2 temp = transform.position;
            temp.y -= speed * Time.deltaTime;
            transform.position = temp;

            if(temp.y < bound_Y)
                gameObject.SetActive(false);
        }
    }

    /*void RotateEnemy() {
        if(canRotate) {
            transform.Rotate(new Vector2(0f,0f,rotate_Speed * Time.deltaTime), Space.World);
        }
    }*/
    
    //Tire
    void StartShooting() {

        if (canShoot) {
            GameObject bullet = Instantiate(bulletPrefab, attack_point.position, Quaternion.identity);
            randomtest = Random.Range(1f, 2f);
            Invoke("StartShooting", randomtest);
            Invoke("Anim_shoot", randomtest);
        }

    }

    void OnTriggerEnter2D(Collider2D target) {

        if(target.tag == "Bullet" || target.tag == "Player") {
            GameObject Player = GameObject.Find("Playership");
            canShoot = false;
            anim.SetBool("Enemy_death", true);
            speed = 0f;
            Player.GetComponent<PlayerScore>().SessionKill ++;

            RandomBoost = Random.Range(1f, 100f);
            if (RandomBoost <= 10f && canBoostLife)
            {
                GameObject boostlife = Instantiate(boostlifeprefab, attack_point.position, Quaternion.Euler(0f, 0f, 180f));
            }
            else if (RandomBoost <= 20f && canBoostFire)
            {
                GameObject boostfire = Instantiate(boostfireprefab, attack_point.position, Quaternion.Euler(0f, 0f, 180f));
            }
            else if (RandomBoost <= 30f && canBoostSpeed)
            {
                GameObject boostspeed = Instantiate(boostspeedprefab, attack_point.position, Quaternion.Euler(0f, 0f, 180f));
            }


            Invoke("Destruction", 1f);
        }
    }

    void Destruction()
    {
        Destroy(gameObject);
    }

    void Anim_shoot()
    {
        anim.SetBool("Enemy_fire", true);

    }



} // class








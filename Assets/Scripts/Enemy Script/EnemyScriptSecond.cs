﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScriptSecond : MonoBehaviour
{
    public float speed = 5f;
    //public float rotate_Speed = 50f;
    public float randomtest = 0f;

    public bool canShoot;
    //public bool canRotate;
    public bool canMove = true;

    public float bound_X = -6f;

    public Transform attack_point;
    public GameObject bulletPrefab;

    public Animator anim2;
    private AudioSource explosionSound; //A Monter Dans le Future


    // Start is called before the first frame update
    void Awake()
    {
        anim2 = GetComponent<Animator>();
        explosionSound = GetComponent<AudioSource>();

    }

    void Start()
    {
        /*if (canRotate)
        {
            if (Random.Range(0, 2) > 0)
            {
                rotate_Speed = Random.Range(rotate_Speed, rotate_Speed + 20f);
                rotate_Speed *= -1f;
            }
            else
            {
                rotate_Speed = Random.Range(rotate_Speed, rotate_Speed + 20f);
            }
        }*/

        if (canShoot)
        {
            randomtest = Random.Range(1f, 2f);
            Invoke("StartShooting", randomtest);
            Invoke("Anim2_shoot", randomtest);
        }

    }


    // Update is called once per frame
    void Update()
    {
        Move();
        //RotateEnemy();
        anim2.SetBool("Enemy_Shoot", false);
    }

    void Move()
    {
        if (canMove)
        {
            Vector2 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;

            if (temp.x < bound_X)
                gameObject.SetActive(false);
        }
    }

    /*void RotateEnemy() {
        if(canRotate) {
            transform.Rotate(new Vector2(0f,0f,rotate_Speed * Time.deltaTime), Space.World);
        }
    }*/

    //Tire
    void StartShooting()
    {

        if (canShoot)
        {
            GameObject bullet = Instantiate(bulletPrefab, attack_point.position, Quaternion.Euler(0f, 0f, -90f));
            randomtest = Random.Range(1f, 2f);
            Invoke("StartShooting", randomtest);
            Invoke("Anim2_shoot", randomtest);
        }

    }

    void OnTriggerEnter2D(Collider2D target)
    {

        if (target.tag == "Bullet" || target.tag == "Player")
        {
            GameObject Player = GameObject.Find("Playership");
            canShoot = false;
            transform.Rotate(new Vector3(0f, 0f, 90f), Space.World);
            anim2.SetBool("Enemy_Death", true);
            Player.GetComponent<PlayerScore>().SessionKill ++;
            speed = 0f;
            Invoke("Destruction", 1f);
        }
    }

    void Destruction()
    {
        Destroy(gameObject);
    }

    void Anim2_shoot()
    {
        anim2.SetBool("Enemy_Shoot", true);

    }



}

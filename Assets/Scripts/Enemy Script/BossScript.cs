﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossScript : MonoBehaviour
{
    public float Xspeed = 0f;
    public float Yspeed = 3f;
    public float randomtest = 0f;
    private int life = 100;

    public bool canShoot;
    public bool canMove = true;
    public bool moveRight;
    

    public float bound_Y = 3f;
    public float min_X = -8f, max_X = 8f;
    
    public GameObject boostlifeprefab;
    public GameObject boostspeedprefab;
    public GameObject boostfireprefab;

    public Animator anim;
    
    
    public Transform attack_pointMidle;
    public Transform attack_pointLeft;
    public Transform attack_pointRight;

    public GameObject bulletPrefabMidle;
    public GameObject bulletPrefabLeft;
    public GameObject bulletPrefabRight;


    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();

    }

    void Start()
    {
        moveRight = false;
        Invoke("StartShooting", randomtest);
    }


    // Update is called once per frame
    void Update()
    {
        MoveY();
        MoveX();
        //RotateEnemy();
        anim.SetBool("Boss_fire", false);
    }

    void MoveY()
    {
        if (canMove)
        {
            Vector2 temp = transform.position;
            temp.y -= Yspeed * Time.deltaTime;
            transform.position = temp;

            if (temp.y <= bound_Y)
            {
                Yspeed = 0f;
                Xspeed = 2f;
            }
        }
    }

    void MoveX()
    {
        if (canMove)
        {
            Vector2 temp = transform.position;
            if (moveRight)
            {
                temp.x += Xspeed * Time.deltaTime;
            }
            else
            {
                temp.x -= Xspeed * Time.deltaTime;
            }
            
            transform.position = temp;

            if (temp.x < min_X)
            {
                moveRight = true;
            }else if(temp.x > max_X)
            {
                moveRight = false;
            }
        }
    }

    //Tire
    void StartShooting()
    {
            GameObject bullet = Instantiate(bulletPrefabMidle, attack_pointMidle.position, Quaternion.identity);
            GameObject bullet1 = Instantiate(bulletPrefabLeft, attack_pointLeft.position, Quaternion.Euler(0f, 0f, -45f));
            GameObject bullet2 = Instantiate(bulletPrefabRight, attack_pointRight.position, Quaternion.Euler(0f, 0f, 45f));
            randomtest = Random.Range(0.5f, 2f);
            Invoke("StartShooting", randomtest);
            Invoke("Anim_shoot", randomtest);
    }

    void OnTriggerEnter2D(Collider2D target)
    {

        if (target.tag == "Bullet")
        {
            life--;
            anim.SetBool("Boss_hit", true);
            Invoke("anim_hit", 1f);

            if(life <= 0)
            {
                GameObject Player = GameObject.Find("Playership");
                canShoot = false;
                anim.SetBool("Boss_explode", true);
                canMove = false;
                Player.GetComponent<PlayerScore>().SessionKill += 30;

                Invoke("Destruction", 2f);
            }
        }
    }

    void Destruction()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2);
        Destroy(gameObject);
    }

    void Anim_shoot()
    {
        anim.SetBool("Boss_fire", true);

    }

    void anim_hit()
    {
        anim.SetBool("Boss_hit", false);
    }



}

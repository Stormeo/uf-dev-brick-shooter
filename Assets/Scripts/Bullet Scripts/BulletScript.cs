﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour  {

    public float speed = 5f;
    //public float desactivate_Timer = 3f;
    public float bound_Y = -6f;

    [HideInInspector]
    public bool is_EnemyBullet = false;


    // Start is called before the first frame update
    void Start() {

        if (is_EnemyBullet)
            speed *= -1f;

        //Invoke("DeactivateGameObject", desactivate_Timer);

        

    }

    // Update is called once per frame
    void Update() {
        
        Move();
        
    }

     void Move() {
        Vector3 temp = transform.position;
        temp.y += speed * Time.deltaTime;
        transform.position = temp;

         if(temp.y > bound_Y)
            gameObject.SetActive(false);

    }

    /*void DeactivateGameObject() {
        gameObject.SetActive(false);
    }*/

    void OnTriggerEnter2D(Collider2D target) {

        if(target.tag == "EnemyBullet" || target.tag == "Enemy") {
            gameObject.SetActive(false);
        }
    }



} // class
























﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnnemiScript : MonoBehaviour
{
    public float speed = 5f;
    //public float desactivate_Timer = 3f;
    public float bound_Y = -6f;

    public bool leftBullet;
    public bool rightBullet;


    // Start is called before the first frame update
    void Start()
    {
        speed *= -1f;
        //Invoke("DeactivateGameObject", desactivate_Timer);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        Vector3 temp = transform.position;
        if (!leftBullet && !rightBullet)
        {
            temp.y += speed * Time.deltaTime;
            transform.position = temp;
        }else if (leftBullet)
        {
            temp.y += speed * Time.deltaTime;
            temp.x += speed * Time.deltaTime;
            transform.position = temp;
        }
        else if(rightBullet){
            temp.y += speed * Time.deltaTime;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;
        }

        if (temp.y < bound_Y) 
            gameObject.SetActive(false);

    }

    void OnTriggerEnter2D(Collider2D target)
    {

        if (target.tag == "Bullet" || target.tag == "Player")
        {
            gameObject.SetActive(false);
        }

        /*void DeactivateGameObject() {
            gameObject.SetActive(false);
        }*/



    } // class
}




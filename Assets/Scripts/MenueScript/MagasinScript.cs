﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagasinScript : MonoBehaviour
{
    private int lifeCoup = 100;
    private int bonusLifeCoup = 200;
    private int bonusSpeedCoup = 200;
    private int bonusFireCoup = 200;

    private int point;

    private bool life_sell;
    private bool bonusLife_sell;
    private bool bonusSpeed_sell;
    private bool bonusFire_sell;

    // Start is called before the first frame update
    void Start()
    {
        point = PlayerPrefs.GetInt("Nbkill");

        if(PlayerPrefs.GetInt("life_sellVerif") == 0)
        {
            life_sell = false;
        }
        else
        {
            life_sell = true;
        }

        if (PlayerPrefs.GetInt("bonusLife_sellVerif") == 0)
        {
            bonusLife_sell = false;
        }
        else
        {
            bonusLife_sell = true;
        }

        if (PlayerPrefs.GetInt("bonusSpeed_sellVerif") == 0)
        {
            bonusSpeed_sell = false;
        }
        else
        {
            bonusSpeed_sell = true;
        }

        if (PlayerPrefs.GetInt("bonusFire_sellVerif") == 0)
        {
            bonusFire_sell = false;
        }
        else
        {
            bonusFire_sell = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void achatLife()
    {
        if(!life_sell && point > lifeCoup)
        {
            point -= lifeCoup;
            PlayerPrefs.SetInt("Nbkill", -lifeCoup);
            life_sell = true;
            PlayerPrefs.SetInt("life_sellVerif", 1);
        }
    }

    void achatBonusSpeed()
    {
        if (!bonusSpeed_sell && point > bonusSpeedCoup)
        {
            point -= bonusSpeedCoup;
            PlayerPrefs.SetInt("Nbkill", -bonusSpeedCoup);
            bonusSpeed_sell = true;
            PlayerPrefs.SetInt("bonusSpeed_sellVerif", 1);
        }
    }

    void achatBonusLife()
    {
        if (!bonusLife_sell && point > bonusLifeCoup)
        {
            point -= bonusLifeCoup;
            PlayerPrefs.SetInt("Nbkill", -bonusLifeCoup);
            bonusLife_sell = true;
            PlayerPrefs.SetInt("bonusLife_sellVerif", 1);
        }
    }

     void achatBonusFire()
    {
        if (!bonusFire_sell && point > bonusFireCoup)
        {
            point -= bonusFireCoup;
            PlayerPrefs.SetInt("Nbkill", -bonusFireCoup);
            bonusFire_sell = true;
            PlayerPrefs.SetInt("bonusFire_sellVerif", 1);
        }
    }


}

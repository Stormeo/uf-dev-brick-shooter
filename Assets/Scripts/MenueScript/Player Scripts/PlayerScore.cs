﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    public int SessionKill = 0;
    public int VerifSessionKill;
    public int Globalkill;

    public bool FinalScore;

    public Text killCount;


    // Start is called before the first frame update
    void Start()
    {
        SessionKill = PlayerPrefs.GetInt("Sessionkill");
        Globalkill = PlayerPrefs.GetInt("Nbkill");

        if (FinalScore)
        {
            Globalkill += SessionKill;
            PlayerPrefs.SetInt("Nbkill", Globalkill);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (VerifSessionKill != SessionKill)
        {
            VerifSessionKill = SessionKill;
            PlayerPrefs.SetInt("Sessionkill", SessionKill);
        }

        if (FinalScore)
        {
            killCount.text = "Kill Totale: " + Globalkill.ToString();
        } else
        {
            killCount.text = "Kill: " + SessionKill.ToString();
        }
    }
}

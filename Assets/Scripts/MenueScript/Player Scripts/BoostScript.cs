﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostScript : MonoBehaviour
{
    public float speed = 5f;
    public bool canMove;

    public bool speedBoost;
    public bool fireBoost;
    public bool lifeBoost;

    public float bound_Y = -6f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    void Move()
    {
        if (canMove)
        {
            Vector2 temp = transform.position;
            temp.y -= speed * Time.deltaTime;
            transform.position = temp;

            if (temp.y < bound_Y)
                gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {

        if (target.tag == "Player")
        {
            if (speedBoost){

                GameObject Player = GameObject.Find("Playership");
                speed = 0f;
                if (Player.GetComponent<PlayerController>().speed < 9f)
                    Player.GetComponent<PlayerController>().speed++;
                Invoke("Destruction", 0.2f);

            }
            else if(fireBoost){

                GameObject Player = GameObject.Find("Playership");
                speed = 0f;
                if(Player.GetComponent<PlayerController>().current_Attack_Timer > 0.15f)
                    Player.GetComponent<PlayerController>().current_Attack_Timer -= 0.10f;
                Invoke("Destruction", 0.2f);

            }
            else if (lifeBoost){

                GameObject Player = GameObject.Find("Playership");
                speed = 0f;
                if (Player.GetComponent<PlayerController>().life< 3)
                    Player.GetComponent<PlayerController>().life++;
                Invoke("Destruction", 0.2f);

            }
        }
    }

    void Destruction()
    {
        Destroy(gameObject);
    }

}

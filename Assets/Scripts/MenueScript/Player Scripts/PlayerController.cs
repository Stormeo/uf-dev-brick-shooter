﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public int life = 3;
    public int lifeVerif;

    public float min_X, max_X;
    public float min_Y, max_Y;

    public Animator animator;

    [SerializeField]
    private GameObject player_Bullet;

    [SerializeField]
    private Transform attack_Point;

    public float attack_Timer = 0.35f;
    public float current_Attack_Timer;
    private bool canAttack;
    private bool isInvincible;

    public Text lifeCount;

    // Start is called before the first frame update
    void Start() {

        current_Attack_Timer = attack_Timer;
        lifeCount.text = "Vie: " + life.ToString();
        isInvincible = false;
        lifeVerif = life;
    }

    // Update is called once per frame
    void Update()
    {
        MovePlayer();
        Attack();

        if(lifeVerif != life)
        {
            lifeVerif = life;
            lifeCount.text = "Vie: " + life.ToString();
        }
    }

    void MovePlayer()
    {
        if(Input.GetAxisRaw("Horizontal") > 0f)
        {
            Vector2 temp = transform.position;
            temp.x += speed * Time.deltaTime;

            if (temp.x > max_X)
                temp.x = max_X;

            transform.position = temp;

        }else if (Input.GetAxisRaw("Horizontal") < 0f)
        {
            Vector2 temp = transform.position;
            temp.x -= speed * Time.deltaTime;

            if (temp.x < min_X)
                temp.x = min_X;

            transform.position = temp;
        }
        if (Input.GetAxisRaw("Vertical") > 0f)
        {
            Vector3 temp = transform.position;
            temp.y += speed * Time.deltaTime;

            if (temp.y > max_Y)
                temp.y = max_Y;

            transform.position = temp;

        }
        else if (Input.GetAxisRaw("Vertical") < 0f)
        {
            Vector3 temp = transform.position;
            temp.y -= speed * Time.deltaTime;

            if (temp.y < min_Y)
                temp.y = min_Y;

            transform.position = temp;
        }
    }

    void Attack() {

        attack_Timer += Time.deltaTime;
        if(attack_Timer > current_Attack_Timer) {
            canAttack = true;
        } 

        if(Input.GetKeyDown(KeyCode.Space)) {
            if(canAttack) {
            animator.SetBool("Player_fire", true);
            canAttack = false;
            attack_Timer = 0f;

            Instantiate(player_Bullet, attack_Point.position, Quaternion.identity);

                // play sound FX (ex bruit de lazer)

            }
        }else{
            animator.SetBool("Player_fire", false);
        }

    } // attack}

    void OnTriggerEnter2D(Collider2D target)
    {

        if ((target.tag == "EnemyBullet" || target.tag == "Enemy") && !isInvincible)
        {
            life--;
            lifeVerif = life;
            lifeCount.text = "Vie: " + life.ToString();

            if (life <= 0)
            {
                isInvincible = true;
                canAttack = false;
                animator.SetBool("Player_death", true);
                speed = 0f;
                Invoke("Destruction", 1f);
            }
            else
            {
                isInvincible = true;
                animator.SetBool("Player_hit", true);
                Invoke("Invincible", 2f);
            }
        }
    }

    void Invincible()
    {
        animator.SetBool("Player_hit", false);
        isInvincible = false;
    }

    void Destruction()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -1);
        Destroy(gameObject);
    }


} // class
